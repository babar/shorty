set :user, 'babar'

server 'ruby.ibabar.com', user: fetch(:user), roles: %w{web app db}, ssh_options: { port: 6395 }

set :application, 'shorty'
set :repo_url, 'git@gitlab.com:babar/shorty.git'

set :deploy_to, "/home/#{fetch(:user)}/Sites/#{fetch(:application)}"
set :scm, :git

set :linked_files, fetch(:linked_files, []).push('config/database.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/sockets')

set :log_level, :info

set :default_env, {
  'DATABASE_URL': 'postgres:///sorty_prod'
}

namespace :bundler do
  after :install, :migrate do
    on roles(:db) do
      within release_path do
        execute :rake, 'db:migrate'
      end
    end
  end
end

namespace :deploy do
  def template(from, to = shared_path)
    template_path = "config/#{from}.erb"
    template = ERB.new(File.new(template_path).read).result(binding)
    upload! StringIO.new(template), "#{to}/#{from}"
  end

  desc 'Upload NginX Config'
  task :nginx_conf do
    on roles(:web) do |role|
      template 'nginx.conf'
      execute :sudo, 'service nginx restart'
    end
  end
end
