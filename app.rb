require 'sinatra/base'
require 'slim'
require 'sinatra/activerecord'
require_relative 'models/redirection'

class App < Sinatra::Base
  register Sinatra::ActiveRecordExtension

  get '/' do
    slim :index
  end

  post '/shorten' do
    halt(404) if params['not_so_secret'] != 'Fo9DrWKWsvffYV7r^hbQXAtf7v9ax'

    short_url Redirection.find_or_create_by(long_url: params['url'])
  end

  not_found do
    'What!?'
  end

  get '/:short_url' do
    begin
      redirect Redirection.find_by!(short_url: params[:short_url]).long_url, 301
    rescue
      redirect request.base_url
    end
  end

  private
    def short_url(redirection)
      request.base_url + '/' + redirection.short_url
    end
end
