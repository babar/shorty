class CreateRedirections < ActiveRecord::Migration
  def change
    create_table :redirections do |t|
      t.string :long_url, unique: true
      t.string :short_url, unique: true
      t.timestamps(null: false)

      t.index :long_url, unique: true
      t.index :short_url, unique: true
    end
  end
end
