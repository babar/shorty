class Redirection < ActiveRecord::Base
  validates_uniqueness_of :long_url, :short_url

  before_validation :generate_short_url


  private
    def generate_short_url
      while short_url.nil?
        random_token = SecureRandom.uuid.first(8)
        self.short_url = random_token unless self.class.exists?(short_url: random_token)       
      end
    end
end
